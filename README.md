# Money Transfer

This project was intend to run as single standalone web application. In order to keep it simple, this project has two URL to be accessed by REST request. One to create accounts and the other to transfer the money from one account to another.
The json sample will be describe on the API Section.
The code was wrote using TDD, thus, 100% of classes are covered with 90% of all lines also covered.
For Database was used HSQLDB with hibernate as ORM.

## Stack
- Java 8
- Hibernate
- HSQLDB
- Maven

## Compile and Run
Run ```mvn clean install``` to run tests and generate the executable jar. 
This going to generate ```moneytransfer-1.0-SNAPSHOT-jar-with-dependencies.jar```.
To execute the generated JAR use the following command ```$java -jar moneytransfer-1.0-SNAPSHOT-jar-with-dependencies.jar```.
If for some reason the Server Port is being used you can pass the number as the first parameter like ```$java -jar moneytransfer-1.0-SNAPSHOT-jar-with-dependencies.jar 9090```.

## APIs
##### POST::http://localhost:<port>/api/create-account

###### Request::JSON

``` 
{
    "accountNumber": 12456,
    "initialAmount": 2150.99
}
```

###### Response::JSON

``` 
{
    "id":8,
    "accountNumber":10010,
    "amount":2150
}
```

##### POST::http://localhost:<port>/api/transfer-money

###### Request::JSON

```
{ 
    "senderAccount": 123456, 
    "receiverAccount": 145678,  
    "amountToBeTransferred": "510.99"
}
```

###### Response::String

```
    "Transferred Completed"
```