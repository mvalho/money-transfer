package com.mvalho.moneytransfer.handler;

import com.mvalho.moneytransfer.domain.Account;
import com.sun.net.httpserver.HttpExchange;

import java.io.InputStream;

public interface CreateAccountHandler extends BaseHandler {
    Account createAccount(InputStream requestBody);

    void handlerResponse(HttpExchange httpExchange, Account account);
}
