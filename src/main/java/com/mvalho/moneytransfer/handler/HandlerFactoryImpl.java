package com.mvalho.moneytransfer.handler;

import com.mvalho.moneytransfer.dao.AccountDAOImpl;
import com.mvalho.moneytransfer.dao.ConnectionUtilImpl;
import com.mvalho.moneytransfer.service.AccountService;
import com.mvalho.moneytransfer.service.AccountServiceImpl;
import com.sun.net.httpserver.HttpExchange;
import lombok.extern.java.Log;

@Log
public class HandlerFactoryImpl implements HandlerFactory {

    private static final String CREATE_ACCOUNT = "CREATE_ACCOUNT";
    private static final String TRANSFER_MONEY = "TRANSFER_MONEY";
    private AccountService accountService;

    HandlerFactoryImpl() {
        this.accountService = new AccountServiceImpl(new AccountDAOImpl(new ConnectionUtilImpl()));
    }

    @Override
    public BaseHandler getHandler(HttpExchange httpExchange) {
        log.info("Handle Factory getting handler");
        String handleType = getHandlerType(httpExchange);

        switch (handleType) {
            case CREATE_ACCOUNT:
                return new CreateAccountHandlerImpl(accountService);
            case TRANSFER_MONEY:
                return new TransferMoneyHandlerImpl(accountService);
            default:
                return new ErrorHandlerImpl();
        }
    }

    private String getHandlerType(HttpExchange httpExchange) {
        String url = httpExchange.getRequestURI().toString();

        if(url.matches("/api/create-account")) {
            return CREATE_ACCOUNT;
        } else if (url.matches("/api/transfer-money")) {
            return TRANSFER_MONEY;
        }

        return "";
    }
}
