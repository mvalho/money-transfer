package com.mvalho.moneytransfer.handler;

import com.mvalho.moneytransfer.domain.Account;
import com.sun.net.httpserver.HttpExchange;
import lombok.extern.java.Log;

import java.io.InputStream;

@Log
public class MainHandlerImpl implements MainHandler {
    @Override
    public void handle(HttpExchange httpExchange) {
        log.info("Handling Request");
        HandlerFactory handlerFactory = new HandlerFactoryImpl();
        BaseHandler baseHandler = handlerFactory.getHandler(httpExchange);

        if(baseHandler instanceof ErrorHandler) {
            ErrorHandler errorHandler = (ErrorHandler) baseHandler;
            errorHandler.handlerResponse(httpExchange);
            return;
        }

        baseHandler.isMethodTypeValidForThisHandler(httpExchange.getRequestMethod());
        baseHandler.isURLValidForMethodType(httpExchange.getRequestURI(), httpExchange.getRequestMethod());
        InputStream requestBody = httpExchange.getRequestBody();

        if(baseHandler instanceof CreateAccountHandler) {
            CreateAccountHandler createAccountHandler = (CreateAccountHandler) baseHandler;
            Account account = createAccountHandler.createAccount(requestBody);
            createAccountHandler.handlerResponse(httpExchange, account);
        } else if(baseHandler instanceof  TransferMoneyHandler) {
            TransferMoneyHandler transferMoneyHandler = (TransferMoneyHandler) baseHandler;
            boolean isTransferred = transferMoneyHandler.transferMoney(requestBody);
            transferMoneyHandler.handlerResponse(httpExchange, isTransferred);
        }
    }
}
