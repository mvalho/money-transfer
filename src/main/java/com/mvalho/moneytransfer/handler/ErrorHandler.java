package com.mvalho.moneytransfer.handler;

import com.sun.net.httpserver.HttpExchange;

public interface ErrorHandler extends BaseHandler {
    void handlerResponse(HttpExchange httpExchange);
}
