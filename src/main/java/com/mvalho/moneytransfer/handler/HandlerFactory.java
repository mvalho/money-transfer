package com.mvalho.moneytransfer.handler;

import com.sun.net.httpserver.HttpExchange;

public interface HandlerFactory {
    BaseHandler getHandler(HttpExchange httpExchange);
}
