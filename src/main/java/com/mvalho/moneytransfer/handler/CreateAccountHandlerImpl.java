package com.mvalho.moneytransfer.handler;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.mvalho.moneytransfer.domain.Account;
import com.mvalho.moneytransfer.dto.CreateAccountDTO;
import com.mvalho.moneytransfer.service.AccountService;
import com.sun.net.httpserver.HttpExchange;
import lombok.extern.java.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

@Log
public class CreateAccountHandlerImpl implements CreateAccountHandler {
    private final String[] validMethodTypesForThisHandler = {"POST"};
    private final Gson gson;
    private AccountService accountService;
    private final Map<String, String> methodTypeForURL = new HashMap<>();

    CreateAccountHandlerImpl() {
        this.methodTypeForURL.put("POST", "/api/create-account");
        this.gson = new Gson();
    }

    CreateAccountHandlerImpl(AccountService accountService) {
        this();
        this.accountService = accountService;
    }

    @Override
    public void handlerResponse(HttpExchange httpExchange, Account account) {
        log.info("Create Account - Handling Response for " + account);
        try(OutputStream os = httpExchange.getResponseBody()) {
            if(account != null) {
                String accountJson = this.gson.toJson(account);
                httpExchange.sendResponseHeaders(200, accountJson.length());
                os.write(accountJson.getBytes());
            } else {
                String message = "Cannot create account - " + account.getAccountNumber();
                httpExchange.sendResponseHeaders(400, message.length());
                os.write(message.getBytes());
            }
        } catch (IOException io) {
            io.printStackTrace();
        }
    }

    @Override
    public boolean isMethodTypeValidForThisHandler(String requestMethod) {
        for (String validType : validMethodTypesForThisHandler) {
            if(validType.equalsIgnoreCase(requestMethod)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean isURLValidForMethodType(URI requestURI, String requestMethod) {
        String validUrl = this.methodTypeForURL.get(requestMethod);
        return validUrl != null && validUrl.matches(requestURI.toString());
    }

    @Override
    public Account createAccount(InputStream requestBody) {
        try(InputStreamReader reader = new InputStreamReader(requestBody, StandardCharsets.UTF_8)) {
            BufferedReader bufferedReader = new BufferedReader(reader);
            JsonReader jsonReader = new JsonReader(bufferedReader);
            CreateAccountDTO accountDTO = this.gson.fromJson(jsonReader, CreateAccountDTO.class);

            Account account = accountDTO.createAccount();

            if(!this.accountService.checkAccount(account.getAccountNumber())) {
                log.info("Create Account - creating account " + account);
                this.accountService.createAccount(account);
            }

            return account;
        } catch (Exception e) {
            log.info("Create Account - Handling Response for " + e.getMessage());
            return null;
        }
    }
}
