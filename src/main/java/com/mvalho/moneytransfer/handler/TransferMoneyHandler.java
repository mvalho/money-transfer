package com.mvalho.moneytransfer.handler;

import com.sun.net.httpserver.HttpExchange;

import java.io.InputStream;

interface TransferMoneyHandler extends BaseHandler {
    boolean transferMoney(InputStream requestBody);

    void handlerResponse(HttpExchange httpExchange, boolean isTransferred);
}
