package com.mvalho.moneytransfer.handler;

import com.sun.net.httpserver.HttpExchange;

public interface MainHandler {
    void handle(HttpExchange httpExchange);
}
