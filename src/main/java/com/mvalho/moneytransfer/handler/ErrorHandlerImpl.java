package com.mvalho.moneytransfer.handler;

import com.mvalho.moneytransfer.exception.MethodNotAllowedException;
import com.sun.net.httpserver.HttpExchange;
import lombok.extern.java.Log;

import java.io.IOException;
import java.net.URI;

@Log
public class ErrorHandlerImpl implements ErrorHandler {
    @Override
    public boolean isMethodTypeValidForThisHandler(String requestMethod) {
        throw new MethodNotAllowedException("This method is not allowed for this implementation");
    }

    @Override
    public boolean isURLValidForMethodType(URI requestURI, String requestMethod) {
        throw new MethodNotAllowedException("This method is not allowed for this implementation");
    }

    @Override
    public void handlerResponse(HttpExchange httpExchange) {
        log.info("Error - Invalid Request ");
        try {
            httpExchange.sendResponseHeaders(404, 0);
            httpExchange.getResponseBody().close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
