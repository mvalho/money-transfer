package com.mvalho.moneytransfer.handler;

import java.net.URI;

interface BaseHandler {
    boolean isMethodTypeValidForThisHandler(String requestMethod);

    boolean isURLValidForMethodType(URI requestURI, String requestMethod);
}
