package com.mvalho.moneytransfer.handler;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.mvalho.moneytransfer.dto.TransferMoneyDTO;
import com.mvalho.moneytransfer.service.AccountService;
import com.sun.net.httpserver.HttpExchange;
import lombok.extern.java.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

@Log
public class TransferMoneyHandlerImpl implements TransferMoneyHandler {
    private final String[] validMethodTypesForThisHandler = {"POST"};
    private final Map<String, String> methodTypeForURL = new HashMap<>();
    private final Gson gson = new Gson();

    private AccountService accountService;

    TransferMoneyHandlerImpl() {
        this.methodTypeForURL.put("POST", "/api/transfer-money");
    }

    TransferMoneyHandlerImpl(AccountService accountService) {
        this();
        this.accountService = accountService;
    }

    @Override
    public boolean isMethodTypeValidForThisHandler(String requestMethod) {
        for (String validType : validMethodTypesForThisHandler) {
            if (validType.equalsIgnoreCase(requestMethod)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean isURLValidForMethodType(URI requestURI, String requestMethod) {
        String validUrl = this.methodTypeForURL.get(requestMethod);
        return validUrl != null && validUrl.matches(requestURI.toString());
    }

    @Override
    public boolean transferMoney(InputStream requestBody) {
        try (InputStreamReader reader = new InputStreamReader(requestBody, StandardCharsets.UTF_8)) {
            BufferedReader bufferedReader = new BufferedReader(reader);
            JsonReader jsonReader = new JsonReader(bufferedReader);
            TransferMoneyDTO transferMoneyDTO = this.gson.fromJson(jsonReader, TransferMoneyDTO.class);

            if( !this.accountService.checkAccount(transferMoneyDTO.getSenderAccount()) ||
            !this.accountService.checkAccount(transferMoneyDTO.getReceiverAccount()) ) {
                return false;
            }

            log.info("Transfer Money - transferring " + transferMoneyDTO.getAmountToBeTransferred() + " from " + transferMoneyDTO.getSenderAccount() + " to " + transferMoneyDTO.getReceiverAccount() );
            this.accountService.transferMoney(transferMoneyDTO.getSenderAccount(), transferMoneyDTO.getReceiverAccount(), transferMoneyDTO.getAmountToBeTransferred());

            return true;
        } catch (Exception e) {
            log.info("Money Transfer - Handling Response for " + e.getMessage());
            return false;
        }
    }

    @Override
    public void handlerResponse(HttpExchange httpExchange, boolean isTransferred) {
        log.info("Transfer Money - handling response");

        try(OutputStream os = httpExchange.getResponseBody()) {
            String responseJson;
            if(isTransferred) {
                responseJson = this.gson.toJson("Transferred Completed");
                httpExchange.sendResponseHeaders(200, responseJson.length());
            } else {
                responseJson = this.gson.toJson("Transferred Not Successful");
                httpExchange.sendResponseHeaders(400, responseJson.length());
            }

            os.write(responseJson.getBytes());
        } catch (IOException io) {
            io.printStackTrace();
        }

    }
}
