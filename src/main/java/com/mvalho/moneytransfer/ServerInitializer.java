package com.mvalho.moneytransfer;

import com.mvalho.moneytransfer.handler.MainHandlerImpl;
import com.sun.net.httpserver.HttpServer;
import lombok.extern.java.Log;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.concurrent.Executors;

@Log
public class ServerInitializer {
    public static void main(String[] args) throws IOException {
        int port = args.length == 1 ? Integer.parseInt(args[0]) : 8585;
        log.info("STARTING SERVER AT PORT " + port);
        HttpServer server = HttpServer.create(new InetSocketAddress(port), 0);
        server.createContext("/", new MainHandlerImpl()::handle);
        server.setExecutor(Executors.newCachedThreadPool());
        server.start();
    }
}
