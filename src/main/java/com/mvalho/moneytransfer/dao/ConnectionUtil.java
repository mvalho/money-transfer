package com.mvalho.moneytransfer.dao;

import org.hibernate.SessionFactory;

interface ConnectionUtil {
    SessionFactory getSessionFactory();
}
