package com.mvalho.moneytransfer.dao;

import com.mvalho.moneytransfer.domain.Account;
import lombok.extern.java.Log;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.math.BigDecimal;
import java.util.Optional;

@Log
public class AccountDAOImpl implements AccountDAO {
    private ConnectionUtil connectionUtil;

    AccountDAOImpl() {}

    public AccountDAOImpl(ConnectionUtil connectionUtil) {
        this.connectionUtil = connectionUtil;
    }

    @Override
    public boolean save(Account account) {
        log.info("Saving: " + account);
        Transaction transaction = null;
        try(Session session = connectionUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            session.persist(account);
            session.flush();
            transaction.commit();

            return true;
        } catch (Exception e) {
            if(transaction != null) {
                transaction.rollback();
            }

            e.printStackTrace();

            return false;
        }
    }

    @Override
    public Optional<Account> findByAccountNumber(Long accountNumber) {
        log.info("findByAccountNumber: " + accountNumber);
        Optional<Account> account = Optional.empty();
        try(Session session = connectionUtil.getSessionFactory().openSession()) {
            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<Account> criteriaQuery = criteriaBuilder.createQuery(Account.class);
            Root<Account> from = criteriaQuery.from(Account.class);
            criteriaQuery.select(from).where(criteriaBuilder.equal(from.get("accountNumber"), accountNumber));

            Query<Account> query = session.createQuery(criteriaQuery);

            account = Optional.of(query.uniqueResult());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return account;
    }

    @Override
    public boolean update(Account account) {
        log.info("Updating: " + account);
        Transaction transaction = null;
        try(Session session = connectionUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            session.merge(account);
            session.flush();
            transaction.commit();

            return true;
        } catch (Exception e) {
            if(transaction != null) {
                transaction.rollback();
            }

            e.printStackTrace();

            return false;
        }
    }

    @Override
    public BigDecimal checkBalance(Long accountNumber) {
        log.info("Checking balance for : " + accountNumber);
        BigDecimal balance = null;
        try(Session session = connectionUtil.getSessionFactory().openSession()) {
            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<BigDecimal> criteriaQuery = criteriaBuilder.createQuery(BigDecimal.class);
            Root<Account> from = criteriaQuery.from(Account.class);
            criteriaQuery.select(from.get("amount")).where(criteriaBuilder.equal(from.get("accountNumber"), accountNumber));

            Query<BigDecimal> query = session.createQuery(criteriaQuery);

            balance = query.uniqueResult();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return balance;
    }

    @Override
    public boolean checkAccount(Long accountNumber) {
        log.info("checking account number : " + accountNumber);
        try(Session session = connectionUtil.getSessionFactory().openSession()) {
            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
            Root<Account> from = criteriaQuery.from(Account.class);
            criteriaQuery.select(criteriaBuilder.count(from)).where(criteriaBuilder.equal(from.get("accountNumber"), accountNumber));

            Query<Long> query = session.createQuery(criteriaQuery);

            Long count = query.getSingleResult();

            return count == 1;
        } catch (Exception e) {
            e.printStackTrace();

            return false;
        }
    }
}
