package com.mvalho.moneytransfer.dao;

import com.mvalho.moneytransfer.domain.Account;

import java.math.BigDecimal;
import java.util.Optional;

public interface AccountDAO {
    boolean save(Account account);

    Optional<Account> findByAccountNumber(Long accountNumber);

    boolean update(Account account);

    BigDecimal checkBalance(Long accountNumber);

    boolean checkAccount(Long accountNumber);
}
