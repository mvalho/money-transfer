package com.mvalho.moneytransfer.service;

import com.mvalho.moneytransfer.dao.AccountDAO;
import com.mvalho.moneytransfer.domain.Account;
import lombok.extern.java.Log;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Optional;

@Log
public class AccountServiceImpl implements AccountService {
    private AccountDAO accountDAO;

    AccountServiceImpl() {
    }

    public AccountServiceImpl(AccountDAO accountDAO) {
        this.accountDAO = accountDAO;
    }

    @Override
    @Transactional
    public void transferMoney(Long senderAccountNumber, Long receiverAccountNumber, BigDecimal amountToTransfer) {
        log.info("transferMoney " + amountToTransfer + " from " + senderAccountNumber + " to " + receiverAccountNumber);
        Optional<Account> sender = this.accountDAO.findByAccountNumber(senderAccountNumber);
        Optional<Account> receiver = this.accountDAO.findByAccountNumber(receiverAccountNumber);

        sender.ifPresent(account -> {
            account.debit(amountToTransfer);
            this.accountDAO.update(account);
        });

        receiver.ifPresent(account -> {
            account.credit(amountToTransfer);
            this.accountDAO.update(account);
        });
    }

    @Override
    @Transactional
    public BigDecimal checkBalance(Long accountNumber) {
        return this.accountDAO.checkBalance(accountNumber);
    }

    @Override
    @Transactional
    public boolean checkAccount(Long accountNumber) {
        return this.accountDAO.checkAccount(accountNumber);
    }

    @Override
    @Transactional
    public boolean createAccount(Account account) {
        return this.accountDAO.save(account);
    }
}
