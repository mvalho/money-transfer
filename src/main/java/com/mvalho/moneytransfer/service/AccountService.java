package com.mvalho.moneytransfer.service;

import com.mvalho.moneytransfer.domain.Account;

import java.math.BigDecimal;

public interface AccountService {
    void transferMoney(Long accountNumber, Long accountNumber1, BigDecimal amountToTransfer);

    BigDecimal checkBalance(Long accountNumber);

    boolean checkAccount(Long accountNumber);

    boolean createAccount(Account account);
}
