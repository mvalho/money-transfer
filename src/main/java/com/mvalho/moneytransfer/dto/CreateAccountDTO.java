package com.mvalho.moneytransfer.dto;

import com.mvalho.moneytransfer.domain.Account;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.math.BigDecimal;

@Getter
@AllArgsConstructor
public class CreateAccountDTO {
    private Long accountNumber;
    private String initialAmount;

    public Account createAccount() {
        return new Account(null, this.accountNumber, new BigDecimal(this.initialAmount));
    }
}
