package com.mvalho.moneytransfer.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.math.BigDecimal;

@Getter
@AllArgsConstructor
public class TransferMoneyDTO {
    private Long senderAccount;
    private Long receiverAccount;
    private BigDecimal amountToBeTransferred;
}
