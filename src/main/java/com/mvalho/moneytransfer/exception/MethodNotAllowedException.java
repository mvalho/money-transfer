package com.mvalho.moneytransfer.exception;

public class MethodNotAllowedException extends RuntimeException {
    public MethodNotAllowedException(String s) {
        super(s);
    }
}
