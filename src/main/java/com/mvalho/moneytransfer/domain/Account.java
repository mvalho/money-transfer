package com.mvalho.moneytransfer.domain;

import com.mvalho.moneytransfer.exception.InsufficientBalanceException;
import lombok.*;
import lombok.extern.java.Log;

import javax.persistence.*;
import java.math.BigDecimal;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@EqualsAndHashCode(exclude = {"amount"})
@ToString
@Log
@Entity
@Table(name = "account")
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "account_number")
    private Long accountNumber;

    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    @Column(name = "amount")
    private BigDecimal amount;

    public void debit(BigDecimal amountToTransfer) {
        log.info("Debiting " + amountToTransfer + " from account " + accountNumber);
        BigDecimal newAmount = this.amount.subtract(amountToTransfer);

        if(newAmount.signum() == -1) {
            throw new InsufficientBalanceException("Insufficient Balance for Account " + this.accountNumber);
        }

        this.amount = newAmount;
    }

    public void credit(BigDecimal amountToTransfer) {
        log.info("Crediting " + amountToTransfer + " to account " + accountNumber);

        this.amount = this.amount.add(amountToTransfer);

    }
}
