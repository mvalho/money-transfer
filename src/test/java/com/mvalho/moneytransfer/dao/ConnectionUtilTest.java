package com.mvalho.moneytransfer.dao;

import org.hibernate.SessionFactory;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class ConnectionUtilTest {
    @Test
    public void givenTheSystemStillDoesNotHaveConnectionDAOUtil_whenCreateIt_thenTheInstanceMustBeConnectionDAOUtilImpl() {
        ConnectionUtil connection = new ConnectionUtilImpl();

        assertThat(connection).isNotNull().isInstanceOf(ConnectionUtilImpl.class);
    }

    @Test
    public void givenTheNeededToGetASessionFactory_whenCallGetSessionFactory_thenReturnANewOne() {
        ConnectionUtil connectionUtil = new ConnectionUtilImpl();
        assertThat(connectionUtil.getSessionFactory()).isNotNull().isInstanceOf(SessionFactory.class);
    }
}
