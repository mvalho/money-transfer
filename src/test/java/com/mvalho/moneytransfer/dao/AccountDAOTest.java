package com.mvalho.moneytransfer.dao;

import com.mvalho.moneytransfer.domain.Account;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Optional;

import static org.assertj.core.api.Assertions.anyOf;
import static org.assertj.core.api.Assertions.assertThat;

public class AccountDAOTest {
    @Test
    public void givenTheSystemNeedToSaveAnAccount_thenCreateANewInterfaceToBeCalledToSaveAccount() {
        AccountDAO accountDAO = new AccountDAOImpl();

        assertThat(accountDAO).isNotNull().isInstanceOf(AccountDAOImpl.class);
    }

    @Test
    public void givenTheAccountDaoNeedToSaveAccount_whenCallSaveMethodWithANewAccount_thenTheAccountWillHaveAnId() {
        //given
        AccountDAO accountDAO = new AccountDAOImpl(new ConnectionUtilImpl());
        Account account = new Account(null, 1000L, new BigDecimal(150));

        //when
        boolean isSaved = accountDAO.save(account);

        //then
        assertThat(isSaved).isTrue();
        assertThat(account).isNotNull().hasFieldOrProperty("id").isNotNull();
    }

    @Test
    public void givenTheAccountDAONeedsToRetrieveAccountByItsAccountNumber_whenCallFindByAccountNumber_thenReturnTheAccount() {
        //given
        AccountDAO accountDAO = new AccountDAOImpl(new ConnectionUtilImpl());
        long accountNumber = 2000L;
        Account expectedAccount = new Account(null, accountNumber, new BigDecimal(150));
        accountDAO.save(expectedAccount);

        //when
        Optional<Account> optional = accountDAO.findByAccountNumber(accountNumber);

        //then
        Account account = optional.orElse(null);
        assertThat(account).isNotNull().isEqualTo(expectedAccount).hasNoNullFieldsOrProperties();
    }

    @Test
    public void givenTheAccountDAONeedsToUpdateAccount_whenUpdateAnExistAccount_thenUpdateIsSuccess() {
        //given
        AccountDAO accountDAO = new AccountDAOImpl(new ConnectionUtilImpl());
        long accountNumber = 3000L;
        Account expectedAccount = new Account(null, accountNumber, new BigDecimal(150));
        accountDAO.save(expectedAccount);

        //when
        expectedAccount.debit(new BigDecimal(50));
        boolean isUpdated = accountDAO.update(expectedAccount);

        //then
        assertThat(isUpdated).isTrue();
        Optional<Account> optional = accountDAO.findByAccountNumber(accountNumber);
        Account account = optional.orElse(null);

        assertThat(account).isNotNull().isEqualTo(expectedAccount).hasNoNullFieldsOrProperties();
    }

    @Test
    public void givenAValidAccountNumber_whenCheckBalance_thenReturnTheAmountForThatAccount() {
        //given
        AccountDAO accountDAO = new AccountDAOImpl(new ConnectionUtilImpl());
        BigDecimal amount = new BigDecimal(1250).setScale(2, BigDecimal.ROUND_UNNECESSARY);
        Account account = new Account(null, 4000L, amount);
        accountDAO.save(account);

        //when
        BigDecimal balance = accountDAO.checkBalance(account.getAccountNumber());

        //then
        assertThat(balance).isEqualTo(amount);
    }

    @Test
    public void givenAnNonexistentAccountNumberOnDatabase_whenCheckAccount_thenReturnFalse() {
        //give
        AccountDAO accountDAO = new AccountDAOImpl(new ConnectionUtilImpl());
        Long accountNumber = 3001L;

        //when
        boolean isExistent = accountDAO.checkAccount(accountNumber);

        //then
        assertThat(isExistent).isFalse();
    }

    @Test
    public void givenAnExistentAccountNumberOnDatabase_whenCheckAccount_thenReturnTrue() {
        //give
        AccountDAO accountDAO = new AccountDAOImpl(new ConnectionUtilImpl());
        Long accountNumber = 3002L;
        Account account = new Account(null, accountNumber, new BigDecimal(1500));
        accountDAO.save(account);

        //when
        boolean isExistent = accountDAO.checkAccount(accountNumber);

        //then
        assertThat(isExistent).isTrue();
    }
}
