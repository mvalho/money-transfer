package com.mvalho.moneytransfer;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.mvalho.moneytransfer.dao.AccountDAO;
import com.mvalho.moneytransfer.dao.AccountDAOImpl;
import com.mvalho.moneytransfer.dao.ConnectionUtilImpl;
import com.mvalho.moneytransfer.domain.Account;
import com.mvalho.moneytransfer.dto.CreateAccountDTO;
import com.mvalho.moneytransfer.dto.TransferMoneyDTO;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

import static org.assertj.core.api.Assertions.assertThat;

public class ServerInitializerTest {
    private final Gson gson = new Gson();
    private static String url;

    @BeforeClass
    public static void init() throws IOException {
        String port = "8181";
        url = "http://localhost:" + port;
        ServerInitializer.main(new String[]{port});
    }

    @Test
    public void givenServerInitializer_whenServerIsStarted_thenResponseShouldBe404() throws IOException {
        //when
        HttpURLConnection con = (HttpURLConnection) new URL(url).openConnection();
        con.setRequestMethod("GET");
        int responseCode = con.getResponseCode();
        con.disconnect();

        //then
        assertThat(responseCode).isEqualTo(404);
    }

    @Test
    public void givenServerInitializer_whenServerIsStartedAndCreateAccountIsCalled_thenResponseCodeIs200AndResponseBodyIsAccountCreated() throws IOException {
        HttpURLConnection con = (HttpURLConnection) new URL(url + "/api/create-account").openConnection();
        con.setDoOutput(true);
        con.setRequestMethod("POST");
        setBody(this.gson.toJson(new CreateAccountDTO(70000L, "1525.99")), con);
        JsonReader jsonReader = new JsonReader(new InputStreamReader(con.getInputStream()));
        Account account = this.gson.fromJson(jsonReader, Account.class);
        int responseCode = con.getResponseCode();

        con.disconnect();

        assertThat(responseCode).isEqualTo(200);
        assertThat(account).isNotNull();
        assertThat(account.getId()).isNotNull().isNotZero().isGreaterThan(0L);
    }

    @Test
    @Ignore("This test is falling in pipeline")
    public void givenServerInitializer_whenServerIsStartedAndTransferMoneyIsCalled_thenResponseCodeIs200AndResponseBodyIsTrue() throws IOException {
        //given
        long accountNumber = 10002L;
        long accountNumber2 = 20002L;

        AccountDAO accountDAO = new AccountDAOImpl(new ConnectionUtilImpl());
        accountDAO.save(new Account(null, accountNumber, new BigDecimal(1000)));
        accountDAO.save(new Account(null, accountNumber2, new BigDecimal(1000)));

        //when
        HttpURLConnection connection = (HttpURLConnection) new URL(url + "/api/transfer-money").openConnection();
        connection.setDoOutput(true);
        connection.setRequestMethod("POST");
        setBody(this.gson.toJson(new TransferMoneyDTO(accountNumber, accountNumber2, new BigDecimal("500"))), connection);
        JsonReader jsonReader = new JsonReader(new InputStreamReader(connection.getInputStream()));
        String message  = this.gson.fromJson(jsonReader, String.class);
        int responseCode = connection.getResponseCode();

        connection.disconnect();

        assertThat(responseCode).isEqualTo(200);
        assertThat(message).isEqualTo("Transferred Completed");
    }

    private void setBody(String content, HttpURLConnection con) throws IOException {
        OutputStream os = con.getOutputStream();
        OutputStreamWriter osw = new OutputStreamWriter(os, StandardCharsets.UTF_8);
        osw.write(content);
        osw.flush();
        osw.close();
        os.close();
    }
}
