package com.mvalho.moneytransfer.service;

import com.mvalho.moneytransfer.dao.AccountDAO;
import com.mvalho.moneytransfer.domain.Account;
import com.mvalho.moneytransfer.exception.InsufficientBalanceException;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class AccountServiceTest {
    private Account sender;
    private Account receiver;

    @Before
    public void setup() {
        this.sender = new Account(1L, 1000L, new BigDecimal(1000L));
        this.receiver = new Account(2L, 2000L, new BigDecimal(1000L));
    }

    @Test
    public void givenTheSystemNeedToTransferMoney_thenCreateAccountServiceInterface() {
        AccountService accountService = new AccountServiceImpl();

        assertThat(accountService).isNotNull().isInstanceOf(AccountServiceImpl.class);
    }

    @Test
    public void givenValidAccountNumbersForSenderAndReceiverWithPositiveAmount_whenTransferMoney_thenTheTransactionIsSuccessfully() {
        //given
        AccountDAO accountDAO = mock(AccountDAO.class);
        AccountService accountService = new AccountServiceImpl(accountDAO);
        given(accountDAO.findByAccountNumber(1000L)).willReturn(Optional.of(this.sender));
        given(accountDAO.findByAccountNumber(2000L)).willReturn(Optional.of(this.receiver));
        given(accountDAO.update(this.sender)).willReturn(true);
        given(accountDAO.update(this.receiver)).willReturn(true);
        BigDecimal amountToTransfer = new BigDecimal(500);

        //when
        accountService.transferMoney(this.sender.getAccountNumber(), this.receiver.getAccountNumber(), amountToTransfer);

        //then
        verify(accountDAO, times(2)).findByAccountNumber(anyLong());
        verify(accountDAO, times(2)).update(any(Account.class));
    }

    @Test
    public void givenAnAccountWithPositiveAmount_whenTheAmountToBeTransferredIsBiggerThenBalanceOfSenderAccount_thenThrowAnException() {
        //given
        AccountDAO accountDAO = mock(AccountDAO.class);
        AccountService accountService = new AccountServiceImpl(accountDAO);
        given(accountDAO.findByAccountNumber(1000L)).willReturn(Optional.of(this.sender));
        given(accountDAO.findByAccountNumber(2000L)).willReturn(Optional.of(this.receiver));
        BigDecimal amountToTransfer = new BigDecimal(1001);

        //when
        Throwable throwable = catchThrowable(() -> accountService.transferMoney(this.sender.getAccountNumber(), this.receiver.getAccountNumber(), amountToTransfer));

        //then
        assertThat(throwable).isInstanceOf(InsufficientBalanceException.class)
                .hasMessageContaining("Insufficient Balance for Account " + this.sender.getAccountNumber());
    }

    @Test
    public void givenAnAccountNumberWithPositiveAmount_whenCheckBalance_thenReturnTheAmountForThatAccount() {
        //given
        AccountDAO accountDAO = mock(AccountDAO.class);
        AccountService accountService = new AccountServiceImpl(accountDAO);
        BigDecimal amount = new BigDecimal(1000L);

        given(accountDAO.findByAccountNumber(1000L)).willReturn(Optional.of(this.sender));
        given(accountDAO.checkBalance(this.sender.getAccountNumber())).willReturn(amount);

        //when
        BigDecimal balance = accountService.checkBalance(this.sender.getAccountNumber());

        //then
        assertThat(balance).isEqualTo(amount);
    }
    
    @Test
    public void givenAnNonexistentAccountNumber_whenCheckAccount_thenThrowAnException() {
        //given
        Long accountNumberToBeChecked = 3001L;
        AccountDAO accountDAO = mock(AccountDAO.class);
        AccountService accountService = new AccountServiceImpl(accountDAO);
        given(accountDAO.checkAccount(accountNumberToBeChecked)).willReturn(false);

        //when
        boolean checkAccount = accountService.checkAccount(accountNumberToBeChecked);

        //then
        assertThat(checkAccount).isFalse();
    }

    @Test
    public void givenAnExistentAccountNumber_whenCheckAccount_thenThrowAnException() {
        //given
        Long accountNumberToBeChecked = 3000L;
        AccountDAO accountDAO = mock(AccountDAO.class);
        AccountService accountService = new AccountServiceImpl(accountDAO);
        given(accountDAO.checkAccount(accountNumberToBeChecked)).willReturn(true);

        //when
        Throwable throwable = catchThrowable(() -> accountService.checkAccount(accountNumberToBeChecked));

        //then
        assertThat(throwable).doesNotThrowAnyException();
    }

    @Test
    public void givenAnNonExistentAccount_whenCreateAccount_thenCreateSuccessfullyIt() {
        //given
        long accountNumber = 123450L;
        BigDecimal amount = new BigDecimal(0);
        Account newAccount = new Account(null, accountNumber, amount);
        AccountDAO accountDAO = mock(AccountDAO.class);
        given(accountDAO.save(newAccount)).willReturn(true);
        AccountService accountService = new AccountServiceImpl(accountDAO);

        //when
        boolean isCreated = accountService.createAccount(newAccount);

        //then
        assertThat(isCreated).isTrue();

    }
}
