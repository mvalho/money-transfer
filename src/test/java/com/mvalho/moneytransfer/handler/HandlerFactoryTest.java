package com.mvalho.moneytransfer.handler;

import com.sun.net.httpserver.HttpExchange;
import org.junit.Test;

import java.net.URI;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

public class HandlerFactoryTest {
    @Test
    public void givenTheSystemDoesNotHaveAHandlerFactory_thenCreateIt() {
        HandlerFactory handlerFactory = new HandlerFactoryImpl();

        assertThat(handlerFactory).isNotNull().isInstanceOf(HandlerFactoryImpl.class);
    }

    @Test
    public void givenARequestToCreateAccount_whenGetHandler_thenReturnTheHandlerForThatType() {
        //given
        HttpExchange httpExchange = mock(HttpExchange.class);
        given(httpExchange.getRequestURI()).willReturn(URI.create("/api/create-account"));
        HandlerFactory handlerFactory = new HandlerFactoryImpl();

        //when
        BaseHandler handler = handlerFactory.getHandler(httpExchange);

        //then
        assertThat(handler).isNotNull().isInstanceOf(CreateAccountHandler.class).isInstanceOf(CreateAccountHandlerImpl.class);
    }

    @Test
    public void givenARequestToTransferMoney_whenGetHandler_thenReturnTheHandlerForThatType() {
        //given
        HttpExchange httpExchange = mock(HttpExchange.class);
        given(httpExchange.getRequestURI()).willReturn(URI.create("/api/transfer-money"));
        HandlerFactory handlerFactory = new HandlerFactoryImpl();

        //when
        BaseHandler handler = handlerFactory.getHandler(httpExchange);

        //then
        assertThat(handler).isNotNull().isInstanceOf(TransferMoneyHandler.class).isInstanceOf(TransferMoneyHandlerImpl.class);
    }

    @Test
    public void givenARequestToInvalidURL_whenGetHandler_thenReturnTheHandlerForThatType() {
        //given
        HttpExchange httpExchange = mock(HttpExchange.class);
        given(httpExchange.getRequestURI()).willReturn(URI.create("invalid"));
        HandlerFactory handlerFactory = new HandlerFactoryImpl();

        //when
        BaseHandler handler = handlerFactory.getHandler(httpExchange);

        //then
        assertThat(handler).isNotNull().isInstanceOf(ErrorHandler.class).isInstanceOf(ErrorHandlerImpl.class);
    }
}
