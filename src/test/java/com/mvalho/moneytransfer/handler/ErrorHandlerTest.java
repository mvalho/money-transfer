package com.mvalho.moneytransfer.handler;

import com.mvalho.moneytransfer.exception.MethodNotAllowedException;
import com.sun.net.httpserver.HttpExchange;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

public class ErrorHandlerTest {

    @Test
    public void givenErrorHandlerShouldNotHaveType_whenIsMethodTypeValidForThisHandler_thenThrowException() {
        //given
        ErrorHandler errorHandler = new ErrorHandlerImpl();

        //when
        Throwable throwable = catchThrowable(() -> errorHandler.isMethodTypeValidForThisHandler("any"));

        //then
        assertThat(throwable).hasMessage("This method is not allowed for this implementation").isInstanceOf(MethodNotAllowedException.class);
    }

    @Test
    public void givenErrorHandlerShouldNotURL_whenIsURLValidForMethodType_thenThrowException() {
        //given
        ErrorHandler errorHandler = new ErrorHandlerImpl();

        //when
        Throwable throwable = catchThrowable(() -> errorHandler.isURLValidForMethodType(null, "any"));

        //then
        assertThat(throwable).hasMessage("This method is not allowed for this implementation").isInstanceOf(MethodNotAllowedException.class);
    }


    @Test
    public void givenErrorHandler_whenHandleResponse_thenRespondWith404() throws IOException {
        //given
        HttpExchange httpExchange = mock(HttpExchange.class);
        ErrorHandler errorHandler = new ErrorHandlerImpl();
        ByteArrayOutputStream outputStream = mock(ByteArrayOutputStream.class);
        given(httpExchange.getResponseBody()).willReturn(outputStream);

        //when
        errorHandler.handlerResponse(httpExchange);

        //then
        verify(httpExchange, times(1)).sendResponseHeaders(404, 0);
        verify(httpExchange.getResponseBody(), times(1)).close();
    }
}
