package com.mvalho.moneytransfer.handler;

import com.mvalho.moneytransfer.dao.AccountDAO;
import com.mvalho.moneytransfer.dao.AccountDAOImpl;
import com.mvalho.moneytransfer.dao.ConnectionUtilImpl;
import com.mvalho.moneytransfer.domain.Account;
import com.mvalho.moneytransfer.service.AccountService;
import com.sun.net.httpserver.HttpExchange;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.net.URI;
import java.nio.charset.StandardCharsets;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;

public class MainHandlerTest {
    @Test
    public void givenTheSystemDoesNotHaveAMainHandler_thenCreateIt() {
        MainHandler mainHandler = new MainHandlerImpl();

        assertThat(mainHandler).isNotNull().isInstanceOf(MainHandlerImpl.class);
    }

    @Test
    public void givenMainHandlerMustHandleAnHttpExchangeForCreateAccount_whenHandle_thenHandleTheRequest() {
        //given
        MainHandler mainHandler = new MainHandlerImpl();
        HttpExchange httpExchange = mock(HttpExchange.class);
        given(httpExchange.getRequestURI()).willReturn(URI.create("/api/create-account"));
        given(httpExchange.getRequestBody()).willReturn(new ByteArrayInputStream(StandardCharsets.UTF_8.encode("{ \"accountNumber\":\"1001\", \"initialAmount\":\"1000\" }").array()));
        given(httpExchange.getResponseBody()).willReturn(new ByteArrayOutputStream());

        //when
        mainHandler.handle(httpExchange);

        //then
        verify(httpExchange, times(2)).getRequestMethod();
        verify(httpExchange, times(2)).getRequestURI();
        verify(httpExchange, times(1)).getRequestBody();
    }

    @Test
    public void givenMainHandlerMustHandleAnHttpExchangeForTransferMoney_whenHandle_thenHandleTheRequest() {
        //given
        AccountDAO accountDAO = new AccountDAOImpl(new ConnectionUtilImpl());
        accountDAO.save(new Account(null, 10001L, new BigDecimal(1000)));
        accountDAO.save(new Account(null, 20002L, new BigDecimal(1000)));

        MainHandler mainHandler = new MainHandlerImpl();
        HttpExchange httpExchange = mock(HttpExchange.class);
        given(httpExchange.getRequestURI()).willReturn(URI.create("/api/transfer-money"));
        given(httpExchange.getRequestBody()).willReturn(new ByteArrayInputStream(StandardCharsets.UTF_8.encode("{\"senderAccount\": \"10001\", \"receiverAccount\": \"20002\", \"amountToBeTransferred\": \"510.99\"}").array()));
        given(httpExchange.getResponseBody()).willReturn(new ByteArrayOutputStream());

        //when
        mainHandler.handle(httpExchange);

        //then
        verify(httpExchange, times(2)).getRequestMethod();
        verify(httpExchange, times(2)).getRequestURI();
        verify(httpExchange, times(1)).getRequestBody();
    }

    @Test
    public void givenMainHandlerMustHandleAnHttpExchangeForInvalidRequest_whenHandle_thenHandleTheRequest() {
        //given
        MainHandler mainHandler = new MainHandlerImpl();
        HttpExchange httpExchange = mock(HttpExchange.class);
        given(httpExchange.getRequestURI()).willReturn(URI.create("/api/invalid"));
        given(httpExchange.getRequestBody()).willReturn(new ByteArrayInputStream(StandardCharsets.UTF_8.encode("").array()));
        given(httpExchange.getResponseBody()).willReturn(new ByteArrayOutputStream());

        //when
        mainHandler.handle(httpExchange);

        //then
        verify(httpExchange, times(1)).getResponseBody();
    }
}
