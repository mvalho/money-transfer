package com.mvalho.moneytransfer.handler;

import com.mvalho.moneytransfer.service.AccountService;
import com.sun.net.httpserver.HttpExchange;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.URI;
import java.nio.charset.StandardCharsets;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.anyLong;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class TransferMoneyHandlerTest {
    @Test
    public void givenTheSystemDoesNotHaveAHandlerForCreateAccounts_thenCreateIt() {
        TransferMoneyHandler transferMoneyHandler = new TransferMoneyHandlerImpl();

        assertThat(transferMoneyHandler ).isNotNull().isInstanceOf(TransferMoneyHandlerImpl.class);
    }

    @Test
    public void givenThatBeforeTransferTheMethodTypeShouldBeValid_whenIsMethodTypeValid_thenReturnTrue() {
        //given
        TransferMoneyHandler transferMoneyHandler = new TransferMoneyHandlerImpl();
        HttpExchange httpExchange = mock(HttpExchange.class);
        given(httpExchange.getRequestMethod()).willReturn("POST");

        //when
        boolean isMethodTypeValidForThisHandler = transferMoneyHandler.isMethodTypeValidForThisHandler(httpExchange.getRequestMethod());

        //then
        assertThat(isMethodTypeValidForThisHandler).isTrue();
    }

    @Test
    public void givenThatBeforeTransferTheMethodTypeShouldBeValid_whenIsMethodTypeValid_thenReturnFalse() {
        //given
        TransferMoneyHandler transferMoneyHandler = new TransferMoneyHandlerImpl();
        HttpExchange httpExchange = mock(HttpExchange.class);
        given(httpExchange.getRequestMethod()).willReturn("GET");

        //when
        boolean isMethodTypeValidForThisHandler = transferMoneyHandler.isMethodTypeValidForThisHandler(httpExchange.getRequestMethod());

        //then
        assertThat(isMethodTypeValidForThisHandler).isFalse();
    }

    @Test
    public void givenThatTheURLShouldBeValidForAGivenMethodType_whenIsValidURL_thenReturnTrue() {
        //given
        TransferMoneyHandler transferMoneyHandler = new TransferMoneyHandlerImpl();
        HttpExchange httpExchange = mock(HttpExchange.class);
        given(httpExchange.getRequestURI()).willReturn(URI.create("/api/transfer-money"));
        given(httpExchange.getRequestMethod()).willReturn("POST");

        //when
        boolean isValidURL = transferMoneyHandler.isURLValidForMethodType(httpExchange.getRequestURI(), httpExchange.getRequestMethod());

        //then
        assertThat(isValidURL).isTrue();
    }

    @Test
    public void givenThatTheURLShouldBeValidForAGivenMethodType_whenIsValidURL_thenReturnFalse() {
        //given
        TransferMoneyHandler transferMoneyHandler = new TransferMoneyHandlerImpl();
        HttpExchange httpExchange = mock(HttpExchange.class);
        given(httpExchange.getRequestURI()).willReturn(URI.create("/api/2/transfer-money"));
        given(httpExchange.getRequestMethod()).willReturn("POST");

        //when
        boolean isValidURL = transferMoneyHandler.isURLValidForMethodType(httpExchange.getRequestURI(), httpExchange.getRequestMethod());

        //then
        assertThat(isValidURL).isFalse();
    }

    @Test
    public void givenThatTheURLShouldBeValidForAGivenMethodType_whenMethodIsInvalid_thenReturnFalse() {
        //given
        TransferMoneyHandler transferMoneyHandler = new TransferMoneyHandlerImpl();
        HttpExchange httpExchange = mock(HttpExchange.class);
        given(httpExchange.getRequestURI()).willReturn(URI.create("/api/transfer-money"));
        given(httpExchange.getRequestMethod()).willReturn("GET");

        //when
        boolean isValidURL = transferMoneyHandler.isURLValidForMethodType(httpExchange.getRequestURI(), httpExchange.getRequestMethod());

        //then
        assertThat(isValidURL).isFalse();
    }

    @Test
    public void givenThatAllTheBasicCriteriaWasCheckedBeforeTransferMoney_whenTransferMoney_thenReturnTrue() {
        //given
        AccountService accountService = mock(AccountService.class);
        TransferMoneyHandler transferMoneyHandler = new TransferMoneyHandlerImpl(accountService);
        HttpExchange httpExchange = mock(HttpExchange.class);

        Long senderAccountNumber = 1001L;
        Long receiverAccountNumber = 2002L;

        given(httpExchange.getRequestBody()).willReturn(new ByteArrayInputStream(StandardCharsets.UTF_8.encode("{\"senderAccount\": \"1001\", \"receiverAccount\": \"2002\", \"amountToBeTransferred\": \"510.99\"}").array()));
        given(accountService.checkAccount(senderAccountNumber)).willReturn(true);
        given(accountService.checkAccount(receiverAccountNumber)).willReturn(true);

        //when
        boolean isMoneyTransferred = transferMoneyHandler.transferMoney(httpExchange.getRequestBody());

        //given
        verify(accountService, times(2)).checkAccount(anyLong());
        verify(accountService, times(1)).transferMoney(senderAccountNumber, receiverAccountNumber, new BigDecimal("510.99"));
        assertThat(isMoneyTransferred).isTrue();
    }

    @Test
    public void givenThatTheTransferredWasSuccessfully_whenHandleResponse_thenCreateResponse() throws IOException {
        //given
        HttpExchange httpExchange = mock(HttpExchange.class);
        TransferMoneyHandler transferMoneyHandler = new TransferMoneyHandlerImpl();
        OutputStream outputStream = mock(ByteArrayOutputStream.class);
        given(httpExchange.getResponseBody()).willReturn(outputStream);
        doNothing().when(httpExchange).sendResponseHeaders(eq(200), anyLong());

        //when
        transferMoneyHandler.handlerResponse(httpExchange, true);

        //then
        verify(httpExchange, times(1)).getResponseBody();
        verify(outputStream, times(1)).write(any());
        verify(outputStream, times(1)).close();
    }
}
