package com.mvalho.moneytransfer.handler;

import com.mvalho.moneytransfer.domain.Account;
import com.mvalho.moneytransfer.service.AccountService;
import com.sun.net.httpserver.HttpExchange;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.net.URI;
import java.nio.charset.StandardCharsets;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class CreateAccountHandlerTest {
    @Test
    public void givenTheSystemDoesNotHaveAHandlerForCreateAccounts_thenCreateIt() {
        CreateAccountHandler createAccountHandler = new CreateAccountHandlerImpl();

        assertThat(createAccountHandler).isNotNull().isInstanceOf(CreateAccountHandlerImpl.class);
    }

    @Test
    public void givenThatBeforeCreateNewAccountTheMethodTypeShouldBeValid_whenIsMethodTypeValid_thenReturnTrue() {
        //given
        CreateAccountHandler createAccountHandler = new CreateAccountHandlerImpl();
        HttpExchange httpExchange = mock(HttpExchange.class);
        given(httpExchange.getRequestMethod()).willReturn("POST");

        //when
        boolean isMethodTypeValidForThisHandler = createAccountHandler.isMethodTypeValidForThisHandler(httpExchange.getRequestMethod());

        //then
        assertThat(isMethodTypeValidForThisHandler).isTrue();
    }

    @Test
    public void givenThatBeforeCreateNewAccountTheMethodTypeShouldBeValid_whenIsMethodTypeValid_thenReturnFalse() {
        //given
        CreateAccountHandler createAccountHandler = new CreateAccountHandlerImpl();
        HttpExchange httpExchange = mock(HttpExchange.class);
        given(httpExchange.getRequestMethod()).willReturn("GET");

        //when
        boolean isMethodTypeValidForThisHandler = createAccountHandler.isMethodTypeValidForThisHandler(httpExchange.getRequestMethod());

        //then
        assertThat(isMethodTypeValidForThisHandler).isFalse();
    }

    @Test
    public void givenThatTheURLShouldBeValidForAGivenMethodType_whenIsValidURL_thenReturnTrue() {
        //given
        CreateAccountHandler createAccountHandler = new CreateAccountHandlerImpl();
        HttpExchange httpExchange = mock(HttpExchange.class);
        given(httpExchange.getRequestURI()).willReturn(URI.create("/api/create-account"));
        given(httpExchange.getRequestMethod()).willReturn("POST");

        //when
        boolean isValidURL = createAccountHandler.isURLValidForMethodType(httpExchange.getRequestURI(), httpExchange.getRequestMethod());

        //then
        assertThat(isValidURL).isTrue();
    }

    @Test
    public void givenThatTheURLShouldBeValidForAGivenMethodType_whenIsInvalidURL_thenReturnFalse() {
        //given
        CreateAccountHandler createAccountHandler = new CreateAccountHandlerImpl();
        HttpExchange httpExchange = mock(HttpExchange.class);
        given(httpExchange.getRequestURI()).willReturn(URI.create("/api/2/create-account"));
        given(httpExchange.getRequestMethod()).willReturn("POST");

        //when
        boolean isValidURL = createAccountHandler.isURLValidForMethodType(httpExchange.getRequestURI(), httpExchange.getRequestMethod());

        //then
        assertThat(isValidURL).isFalse();
    }

    @Test
    public void givenThatTheURLShouldBeValidForAGivenMethodType_whenIsMethodIsInvalid_thenReturnFalse() {
        //given
        CreateAccountHandler createAccountHandler = new CreateAccountHandlerImpl();
        HttpExchange httpExchange = mock(HttpExchange.class);
        given(httpExchange.getRequestURI()).willReturn(URI.create("/api/create-account"));
        given(httpExchange.getRequestMethod()).willReturn("GET");

        //when
        boolean isValidURL = createAccountHandler.isURLValidForMethodType(httpExchange.getRequestURI(), httpExchange.getRequestMethod());

        //then
        assertThat(isValidURL).isFalse();
    }

    @Test
    public void givenThatAllTheBasicCriteriaWasCheckedBeforeCreateAccount_whenCreateAccount_thenReturnTrue() {
        //given
        AccountService accountService = mock(AccountService.class);
        CreateAccountHandler createAccountHandler = new CreateAccountHandlerImpl(accountService);
        HttpExchange httpExchange = mock(HttpExchange.class);

        long accountNumber = 1001L;
        Account account = new Account(null, accountNumber, new BigDecimal(1000L));

        given(httpExchange.getRequestBody()).willReturn(new ByteArrayInputStream(StandardCharsets.UTF_8.encode("{ \"accountNumber\":\"1001\", \"initialAmount\":\"1000\" }").array()));
        given(accountService.checkAccount(accountNumber)).willReturn(false);
        given(accountService.createAccount(account)).willReturn(true);

        //when
        Account accountCreated = createAccountHandler.createAccount(httpExchange.getRequestBody());

        //then
        verify(accountService, times(1)).checkAccount(anyLong());
        verify(accountService, times(1)).createAccount(account);
        assertThat(accountCreated).isNotNull();
    }

    @Test
    public void givenThatAllTheBasicCriteriaWasCheckedBeforeCreateAccount_whenCreateAccount_thenReturnFalse() {
        //given
        AccountService accountService = mock(AccountService.class);
        CreateAccountHandler createAccountHandler = new CreateAccountHandlerImpl(accountService);
        HttpExchange httpExchange = mock(HttpExchange.class);

        long accountNumber = 1001L;
        Account account = new Account(null, accountNumber, new BigDecimal(1000L));

        given(httpExchange.getRequestBody()).willReturn(new ByteArrayInputStream(StandardCharsets.UTF_8.encode("{ \"accountNumber\":\"1001\", \"initialAmount\":\"1000\" }").array()));
        given(accountService.checkAccount(accountNumber)).willReturn(false);
        given(accountService.createAccount(account)).willReturn(false);

        //when
        Account accountCreated = createAccountHandler.createAccount(httpExchange.getRequestBody());

        //then
        verify(accountService, times(1)).checkAccount(anyLong());
        verify(accountService, times(1)).createAccount(account);
        assertThat(accountCreated.getId()).isNull();
    }

    @Test
    public void givenThatAnAccountWasCreated_whenHandleResponse_thenCreateResponse() throws IOException {
        //given
        HttpExchange httpExchange = mock(HttpExchange.class);
        CreateAccountHandler createAccountHandler = new CreateAccountHandlerImpl();
        Account accountCreated = new Account(1L, 10001L, new BigDecimal(1000));
        OutputStream outputStream = mock(ByteArrayOutputStream.class);
        given(httpExchange.getResponseBody()).willReturn(outputStream);
        doNothing().when(httpExchange).sendResponseHeaders(eq(200), anyLong());

        //when
        createAccountHandler.handlerResponse(httpExchange, accountCreated);

        //then
        verify(httpExchange, times(1)).getResponseBody();
        verify(outputStream, times(1)).write(any());
        verify(outputStream, times(1)).close();
    }

    @Test
    public void givenThatAnAccountWasNotCreated_whenHandleResponse_thenCreateResponse() throws IOException {
        //given
        HttpExchange httpExchange = mock(HttpExchange.class);
        CreateAccountHandler createAccountHandler = new CreateAccountHandlerImpl();
        OutputStream outputStream = mock(ByteArrayOutputStream.class);
        given(httpExchange.getResponseBody()).willReturn(outputStream);
        doNothing().when(httpExchange).sendResponseHeaders(eq(200), anyLong());
        Account account = new Account(null, 10001L, new BigDecimal(1000));

        //when
        createAccountHandler.handlerResponse(httpExchange, account);

        //then
        verify(httpExchange, times(1)).getResponseBody();
        verify(outputStream, times(1)).write(any());
        verify(outputStream, times(1)).close();
    }
}
